import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button } from 'react-bootstrap';
import axios from 'axios';
import {
    changeTipo, getPessoaJuridica, changePessoaJuridica, changeProcesso,
    changeDescricao, addFileItem, addDocument, changePond, showAlert, TraingDocument, addInput,
    changePalavraChave, deleteInput, setTela, getRepresentacao, changeRepresentacao, changeTitulo, setSucessData, setErrorData, setWarningData, setCorpoDecisaoData, generatePDf
} from './decisaoActions'
import 'filepond/dist/filepond.min.css';
import './Style.css';
import btBack from '../img/bt-voltar-01b.png'
import Loading from '../loading/loading'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { getUrl } from '../../Constantes'
import 'react-notifications/lib/notifications.css';
import CKEditor from '@ckeditor/ckeditor5-react';
import DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document'
const base_url_training = getUrl(window.location.hostname).base_url_training


const config = {
    readonly: false // all options from https://xdsoft.net/jodit/doc/
}
const editorConfiguration = {
    removePlugins: ['paragraph'],

};
class Decisao extends Component {
    getTextEditor(text) {
        const textFormatado = text.replace(/(\n)\1+/g, '\n')
        console.log(JSON.stringify(textFormatado))
        const textFormatado2 = textFormatado.replace(/(?:\r\n|\r|\n)/g, '<p></p>')
        return textFormatado2
    }

    UNSAFE_componentWillMount() {
        this.props.getRepresentacao()
    }
    getMessenger() {
        if (this.props.errorDecisao && this.props.errorDecisao !== "") {
            NotificationManager.error(this.props.errorDecisao, 'Erro :(', 3000)
            this.props.setErrorData("")
        }
        if (this.props.sucessDecisao && this.props.sucessDecisao !== "") {
            console.log("entrou sucess decisao")
            NotificationManager.success(this.props.sucessDecisao, 'Successo :)', 3000)
            this.props.setSucessData("")
        }
        if (this.props.warningDecisao && this.props.warningDecisao !== '') {
            console.log("entrou sucess edital")
            NotificationManager.info(this.props.warningDecisao, 'Dados preenchidos incorretamente. :|', 3000)
            this.props.setWarningData('')
        }
    }
    geraPdf() {
        const data = {
            'gabinete':this.refs.conselheiro.value, 
            'body': this.refs.CK.editor.getData(),
        }
        axios.post(base_url_training+'return-files/',data,{
            method: 'POST',
            responseType: 'blob'})
        .then(function (response) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'file.pdf');
            document.body.appendChild(link);
            link.click();
        })
        .catch(function (error) {
            console.log(error)
        });
    }
    geraWord() {
        const data = {
            'gabinete':this.refs.conselheiro.value, 
            'body': this.refs.CK.editor.getData(),
        }
        axios.post('http://127.13.10.10:5000/return-docx/',data,{
            method: 'POST',
            responseType: 'blob'})
        .then(function (response) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'file.docx');
            document.body.appendChild(link);
            link.click();
        })
        .catch(function (error) {
            console.log(error)
        });
    }
    render() {
        return (
            <div>
                {this.props.loading_decisao === true ? <Loading /> : null}
                {this.getMessenger()}
                <NotificationContainer />
                <div className="btnBack">
                    <button size="sm" variant="outline-primary" className="btnBackStyle" onClick={() => this.props.setTela("/inserir", this.props)}><img alt="" className="btnVoltar" src={btBack} /></button>
                </div>
                <div className="DivDecisao">
                    <div className="divInterna">
                        <Form.Group controlId="formBasicEmail">
                            <Row className="RowElements">
                                <Col xs={5}>
                                    <h4 className='titleDecisao'>Editar Decisão</h4>
                                    <p className='textDecisao'>Faça a edição do texto da decisão</p>
                                </Col>
                            </Row>
                            <Row className="RowElements">
                            <Col xs={12}>
                                    <Form.Label className='textLabel'>Nome do gabinete:</Form.Label>
                                    <Form.Control  ref="conselheiro" className="inputText"  type="text" placeholder="Ex: Gabinete do conselheiro..." />
                                </Col>
                            </Row>
                            <CKEditor
                                className="CkEditor"
                                ref="CK"
                                editor={DecoupledEditor}
                                data={this.getTextEditor(this.props.Decisao_Texto)}
                                onInit={editor => {
                                    console.log('Editor is ready to use!', editor);
                                    // Insert the toolbar before the editable area.
                                    editor.ui.getEditableElement().parentElement.insertBefore(
                                        editor.ui.view.toolbar.element,
                                        editor.ui.getEditableElement()
                                    );
                                }}
                                config={editorConfiguration}
                                onChange={(e) => {
                                    console.log(this.refs.CK.editor.getData())
                                }}
                            />

                            <hr />
                            <Row className="rowSubmit">
                                <div >
                        <Button className="BtnSubmit" onClick={() => this.geraPdf()} variant="primary"  >
                                Gerar PDF
                        </Button>
                        <Button className="BtnSubmit" onClick={() => this.geraWord()} variant="primary"  >
                                Gerar Word
                        </Button>
                                </div>
                            </Row>
                        </Form.Group>
                        <Row className="controlMargin">
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        Decisao_Texto: state.Decisao_Texto.Decisao_Texto,
        generatePdf: state.generatePdf.generatePdf

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changeTipo, getPessoaJuridica, changePessoaJuridica,
        changeProcesso, changeDescricao, addFileItem,
        addDocument, changePond, showAlert, TraingDocument,
        addInput, changePalavraChave, deleteInput, setTela, getRepresentacao, changeRepresentacao, changeTitulo, setSucessData, setErrorData, setWarningData, setCorpoDecisaoData, generatePDf
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Decisao)
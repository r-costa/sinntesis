import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col, Form, Button } from 'react-bootstrap';
import {
    changeTipo, getPessoaJuridica, changePessoaJuridica, changeProcesso,
    changeDescricao, addFileItem, addDocument, changePond, showAlert, TraingDocument, addInput,
    changePalavraChave, deleteInput, setTela, getRepresentacao, changeRepresentacao, changeTitulo, setSucessData, setErrorData,setWarningData
} from './decisaoActions'
import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import './Style.css';
import { getUrl } from '../../Constantes'
import { faTimes, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import btBack from '../img/bt-voltar-01b.png'
import Loading from '../loading/loading'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { Typeahead } from 'react-bootstrap-typeahead';
import TextField from '@material-ui/core/TextField';
import 'react-notifications/lib/notifications.css';

class Decisao extends Component {
    UNSAFE_componentWillMount() {
        this.props.getRepresentacao()
    }
    renderPalavrasChaves() {
        return (
            <div className="rowPalavasChaves">
                <section className="containerPalavraChave flex flex-wrap">

                    {this.props.Decisao_palavras_chaves.map((item, index) => (
                        <div key={index} className="box">
                            <div>
                                <p className="textoPalavraChave">{item.text}</p></div>
                            <div className="divBtn"><Button onClick={() => this.props.deleteInput(index)} className="btnClose" variant="outline-primary"><FontAwesomeIcon className="iconClose" icon={faTimes} /></Button></div>
                        </div>
                    ))}
                </section>
            </div>
        )
    }

    add(event) {
        if (event.key === "Enter") {
            this.props.addInput(this.props.Decisao_palavras_chaves, this.refs.DecisaoinputPalavraChave.value)
            this.refs.DecisaoinputPalavraChave.value = ''
        }
    }
    addPalavraChave() {
        this.props.addInput(this.props.Decisao_palavras_chaves, this.refs.DecisaoinputPalavraChave.value)
        this.refs.DecisaoinputPalavraChave.value = ''
    }
    getMessenger() {
        if (this.props.errorDecisao && this.props.errorDecisao !== "") {
            NotificationManager.error(this.props.errorDecisao, 'Erro :(', 3000)
            this.props.setErrorData("")
        }
        if (this.props.sucessDecisao && this.props.sucessDecisao !== "") {
            console.log("entrou sucess decisao")
            NotificationManager.success(this.props.sucessDecisao, 'Successo :)', 3000)
            this.props.setSucessData("")
        }
        if (this.props.warningDecisao && this.props.warningDecisao !== '') {
            console.log("entrou sucess edital")
            NotificationManager.info(this.props.warningDecisao, 'Dados preenchidos incorretamente. :|', 3000)
            this.props.setWarningData('')
        }
    }

    render() {
        return (
            <div>
                {this.props.loading_decisao === true ? <Loading /> : null}
                {this.getMessenger()}
                <NotificationContainer />
                <div className="btnBack">
                    <button size="sm" variant="outline-primary" className="btnBackStyle" onClick={() => this.props.setTela("/inserir", this.props)}><img alt="" className="btnVoltar" src={btBack} /></button>
                </div>
                {console.log(this.props)}
                <div className="DivDecisao">
                    <div className="divInterna">
                        <Form.Group controlId="formBasicEmail">
                            <Row className="RowElements">

                                <Col xs={5}>
                                    <h4 className='titleDecisao'>Decisão</h4>
                                    <p className='textDecisao'>Faça aqui o Upload de decisões.
                    </p>

                                </Col>

                            </Row>
                            <hr />
                            <Row className="RowElements">
                                <Col xs={3}>
                                    <Form.Label className='textLabel'>Representação:</Form.Label>
                                    <Typeahead
                                        labelKey={(option) => `${option.titulo}`}
                                        id="basic-example"
                                        onChange={(e) => this.props.changeRepresentacao(e)}
                                        options={this.props.Decisao_representacao_data ? this.props.Decisao_representacao_data:null}
                                        placeholder="Selecione o edital"
                                        renderInput={params => <TextField {...params} label="debug" margin="normal" fullWidth />}
                                    />
                                </Col>
                                <Col xs={9}>
                                    <Form.Label className='textLabel'>Título:</Form.Label>
                                    <Form.Control className="inputText" onChange={(e) => this.props.changeTitulo(e)} value={this.props.Decisao_titulo} type="text" placeholder="Digite o título" />
                                </Col>
                            </Row>
                            <Row className="RowElements">
                                <Col xs={12}>
                                    <Form.Label className='textLabel'>Descrição:</Form.Label>
                                    <Form.Control as="textarea" className="textDescricao" onChange={(e) => this.props.changeDescricao(e)} value={this.props.Decisao_Descricao} placeholder="Digite a descrição" />
                                </Col>
                            </Row>
                            {this.props.Decisao_palavras_chaves.length > 0 ?
                                this.renderPalavrasChaves()
                                : null}
                            <Row>
                                <Col className="rowPalavasChaves" xs={4}>
                                    <Form.Label className='textLabel'>Palavra chave:</Form.Label>
                                    <div className="divRowPalavraChave">
                                        <Form.Control ref="DecisaoinputPalavraChave" onKeyPress={(event) => this.add(event, this.props.Decisao_palavras_chaves, this.refs.DecisaoinputPalavraChave.value)} className="inputText" type="email" placeholder={"Palavras chaves"} />
                                        <Button onClick={() => this.addPalavraChave()} size="sm" className="btnPalavraChave" variant="outline-primary"
                                        ><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon></Button>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="filePond">
                                <Col>
                                    <Form.Label className='textLabel'>Upload de arquivos:</Form.Label>
                                    <FilePond
                                        allowMultiple={true}
                                        ref={ref => this.pond = ref}
                                        onupdatefiles={(fileItens) => {
                                            this.props.addFileItem(fileItens)
                                            this.props.changePond(this.pond)
                                        }}
                                        labelIdle='Arraste e solte seus arquivos ou navegue.'
                                        server={
                                            {
                                                url: getUrl(window.location.hostname).url_file_server,
                                                process: {
                                                    headers: {
                                                        'Authorization': "Bearer" + " " + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
                                                    },
                                                },
                                                load:{
                                                    headers: {
                                                        'Authorization': "Bearer" + " " + JSON.parse(window.sessionStorage.getItem('dataUsers')).token
                                                    },
                                                }

                                            }


                                        }/></Col>

                            </Row>
                            <Row className="rowSubmit">
                                <div >
                                    <Button className="BtnSubmit" onClick={() => this.props.addDocument(this.props)} variant="primary"  >
                                        Salvar
                        </Button>
                                </div>
                            </Row>
                        </Form.Group>
                        <Row className="controlMargin">
                        </Row>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        Decisao_tipo_selecionado: state.Decisao_tipo_selecionado.Decisao_tipo_selecionado,
        Decisao_pessoa_juridica: state.Decisao_pessoa_juridica.Decisao_pessoa_juridica,
        Decisao_pessoa_juridica_selecionada: state.Decisao_pessoa_juridica_selecionada.Decisao_pessoa_juridica_selecionada,
        Decisao_processo: state.Decisao_processo.Decisao_processo,
        Decisao_Descricao: state.Decisao_Descricao.Decisao_Descricao,
        Decisao_files: state.Decisao_files.Decisao_files,
        Decisao_pond: state.Decisao_pond.Decisao_pond,
        Decisao_sucess: state.Decisao_sucess.Decisao_sucess,
        Decisao_palavras_chaves: state.Decisao_palavras_chaves.Decisao_palavras_chaves,
        Decisao_representacao_data: state.Decisao_representacao_data.Decisao_representacao_data,
        Decisao_representacao_selecionada: state.Decisao_representacao_selecionada.Decisao_representacao_selecionada,
        Decisao_titulo: state.Decisao_titulo.Decisao_titulo,
        loading_decisao: state.loading_decisao.loading_decisao,
        sucessDecisao: state.sucessDecisao.sucessDecisao,
        errorDecisao: state.errorDecisao.errorDecisao,
        warningDecisao: state.warningDecisao.warningDecisao


    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changeTipo, getPessoaJuridica, changePessoaJuridica,
        changeProcesso, changeDescricao, addFileItem,
        addDocument, changePond, showAlert, TraingDocument,
        addInput, changePalavraChave, deleteInput, setTela, getRepresentacao, changeRepresentacao, changeTitulo, setSucessData, setErrorData,setWarningData
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Decisao)
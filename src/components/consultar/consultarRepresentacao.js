import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setTela,changeRepresentacaoByEditalId,backToLista,setErrorData } from './consultarActions'
import { Row, Col } from 'react-bootstrap';
import RepresentacaoTable from './representacaoTable'
import btBack from '../img/bt-voltar-01b.png'
import 'filepond/dist/filepond.min.css';
import './Style.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';



class ConsultarRepresentacao extends Component {
    componentDidMount(){
        !this.props.listaRepresentacoes || this.props.listaRepresentacoes.length===0?
        this.props.history.push("/lista")
        :null
    }
    getMessenger() {
        if (this.props.consultarError && this.props.consultarError !== '') {
            NotificationManager.info(this.props.consultarError, 'Ops :|', 1200)
            this.props.setErrorData('')
        }
        if (this.props.consultarSucess && this.props.consultarSucess !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.consultarSucess, 'Successo :)', 1200)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                {this.getMessenger()}
                <NotificationContainer />
                <div className="btnBack">
                    {console.log(this.props)}
                    <button size="sm" variant="outline-primary" className="btnBackStyle" onClick={() => this.props.backToLista("/lista")}><img className="btnVoltar" src={btBack} /></button>
                </div>
                <div className="DivConsultar">
                    <div className="divInternaConsultar">
                        <Row className="RowElements">

                            <Col xs={12}>
                                <h5 className='titleEdital'>Representações do edital: {this.props.editalSelecionado.titulo}</h5>
                                <p className='textEdital'>Número do processo: {this.props.editalSelecionado.nrProcesso}
</p>
                            
                            </Col>

                        </Row>
                        <hr />
                        <RepresentacaoTable/>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        editalSelecionado: state.editalSelecionado.editalSelecionado,
        listaRepresentacoes: state.listaRepresentacoes.listaRepresentacoes,
        consultarSucess: state.consultarSucess.consultarSucess,
        consultarError: state.consultarError.consultarError
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setTela,changeRepresentacaoByEditalId,backToLista,setErrorData}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ConsultarRepresentacao)


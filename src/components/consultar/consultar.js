import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getEditais, addConsultar, deleteConsultar, changeTipo, changSeatNumber,
     searchDocuments, changeDocument, setTela ,setErrorData,setLoading} from './consultarActions'
import { Row, Col, Form, Button } from 'react-bootstrap';
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ConsultarTable from './consultarTable'
import btBack from '../img/bt-voltar-01b.png'
import Loading from '../loading/loading'
import 'filepond/dist/filepond.min.css';
import './Style.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';



class Consultar extends Component {
    search(event, inputPesquisa) {
        console.log(event)
        if (event.key === "Enter") {
            if (inputPesquisa !== "") {
                this.props.getEditais(inputPesquisa)
            }
        }
    }
    getMessenger() {
        if (this.props.consultarError && this.props.consultarError !== '') {
            NotificationManager.info(this.props.consultarError, 'Ops :|', 1200)
            this.props.setErrorData('')
        }
        if (this.props.consultarSucess && this.props.consultarSucess !== '') {
            console.log("entrou sucess edital")
            NotificationManager.success(this.props.consultarSucess, 'Successo :)', 1200)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                {this.props.loading === true ? <Loading /> : null}
                {this.getMessenger()}
                <NotificationContainer />
                <div className="btnBack">
                    <button size="sm" variant="outline-primary" className="btnBackStyle" onClick={() => this.props.setTela("/panel")}><img alt="" className="btnVoltar" src={btBack} /></button>
                </div>
                <div className="DivConsultar">
                    <div className="divInternaConsultar">
                        <Row className="RowElements">
                            <Col>
                                <Form.Label className='textLabel'>Pesquisa de editais:</Form.Label>
                                <Form.Control onKeyPress={(event) => this.search(event, this.refs.inputPesquisa.value)} ref="inputPesquisa" className="inputText" value={this.props.Edital_processo} type="text" placeholder="Digite um número de processo ou palavra chave" />
                            </Col>
                            <Col md="auto">
                                <Button title="Buscar edital." className="BtnPesquisa" onClick={() => this.refs.inputPesquisa.value !== "" ? this.props.getEditais(this.refs.inputPesquisa.value) : this.props.setErrorData("Por favor insira um proceso ou palavra chave para a pesquisa.")} variant="primary" type="submit">
                                    <FontAwesomeIcon icon={faSearch}
                                        size="lg">
                                    </FontAwesomeIcon>
                                </Button>
                            </Col>
                        </Row>
                        <ConsultarTable />
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        tipo: state.tipo.tipo,
        document: state.document.document,
        tipo_selecionado: state.tipo_selecionado.tipo_selecionado,
        proximidade: state.proximidade.proximidade,
        document_selecionado: state.document_selecionado.document_selecionado,
        files: state.files.files,
        consultarSucess: state.consultarSucess.consultarSucess,
        consultarError: state.consultarError.consultarError,
        loading: state.loading.loading
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getEditais, changeTipo, searchDocuments, changeDocument, addConsultar, deleteConsultar, changSeatNumber, setTela,setErrorData,setLoading }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Consultar)
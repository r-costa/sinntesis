import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {  Form, Button } from 'react-bootstrap';
import imgPrincipal from'../img/img-login-abertura.png';
import {login,changeLoginData,redirect,setErrorData,setSucessData} from './loginAction'
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import './Style.css';

class Login extends Component {
    loginEnter(event,login,password) {
        console.log(event)
        if (event.key === "Enter") {
            if((login && login!=='') && (password && password !=='')){
                this.props.login(login.toLowerCase(),password)
            }
        }
    }
    componentDidMount() {
        if(!window.sessionStorage.getItem("dataUsers")){
            this.props.changeLoginData(null)
        }
      }
    login(login,password){
        if((login && login!=='') && (password && password !=='')){
            this.props.login(login.toLowerCase(),password)
        }
    }

    getMessenger() {
        console.log(this.props.errorLogin)
        console.log(this.props.sucessLogin)
        if (this.props.errorLogin && this.props.errorLogin !== '') {
            NotificationManager.info(this.props.errorLogin, 'Login incorreto :|', 3000)
            this.props.setErrorData('')
        }
        if (this.props.sucessLogin && this.props.sucessLogin !== '') {
            NotificationManager.success(this.props.sucessLogin, 'Successo :)', 3000)
            this.props.setSucessData('')
        }
    }
    render() {
        return (
            <div>
                    {this.getMessenger()}
                    <NotificationContainer />
                        <div className="DivConsultar">
                            {console.log(JSON.parse(window.sessionStorage.getItem('dataUsers')))}
                        <div>
                            <div className="left">
                                {console.log(this.props)}
                                <div>
                                   <h6 className="textTitleLogin">Seja bem vindo ao sistema Pareceres Data Mining do TCESP, efetue seu login para continuar.</h6> 
                                </div>
                                <div>
                                <Form.Label className='textLabel'>Email</Form.Label>
                                <Form.Control onKeyPress={(event) => this.loginEnter(event, this.refs.inputLogin.value,this.refs.password.value)} autoComplete="off" ref="inputLogin" className="inputLogin" value={this.props.Edital_processo} type="text" placeholder="Digite seu email" />
                                <Form.Label className='textLabel'>Senha</Form.Label>
                                <Form.Control onKeyPress={(event) => this.loginEnter(event, this.refs.inputLogin.value,this.refs.password.value)} autoComplete="off" ref="password" className="inputLogin" value={this.props.Edital_processo} type="password" placeholder="Digite seu senha" />
                                </div>  
                                <div className="rowBtnLogin">
                                    <Button onClick={()=>this.login(this.refs.inputLogin.value,this.refs.password.value)} className="BtnSubmitLogin" variant="primary"><h6 className="textTitleLogin pointer">Login</h6></Button>
                                </div>
                                <div className="novoCadastro">
                                    <h6 onClick={()=>this.props.redirect("/formLogin")} className="textTitleLogin pointer">Ainda não possui cadastro ? Cadastre-se</h6>
                                </div>
                            </div>
                            <div className="right">
                            <img alt="" src={imgPrincipal}/>
                            </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
         LoginComponent: state.LoginComponent.LoginComponent,
         errorLogin: state.errorLogin.errorLogin,
         sucessLogin: state.sucessLogin.sucessLogin
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({login,changeLoginData,redirect,setErrorData,setSucessData}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
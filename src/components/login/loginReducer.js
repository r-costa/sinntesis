const INITIAL_STATE = {
    LoginComponent: null, errorLogin: '', sucessLogin: ''
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_SUCESS_LOGIN':
            return { ...state, sucessLogin: action.payload }
        case 'GET_ERROR_LOGIN':
            console.log("erro")
            return { ...state, errorLogin: action.payload }
        case 'CHANGE_USER_NAME':
            return { ...state, LoginComponent: action.payload }
        case 'CLEAR_USER_NAME':
            return { ...state, LoginComponent: null }
        default:
            return state
    }
}
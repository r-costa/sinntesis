import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-bootstrap';
import logoDataMine from '../img/logo-datamining.png';
import logoTCESP from '../img/logo-tcesp.png';
import LoginComponent from './../login/loginComponent'
import './Style.css';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = { NameLogin: "" };
    }
    render() {
        return (
            <div>

                <Row className='rowHeader' >
                    <Col className='logoEdital' xs={2}>
                        <img alt="" className="imgEiditalView" src={logoDataMine} />
                    </Col>
                    <Col xs={3}></Col>
                    <Col className="loginComponent" xs={2}>
                            <LoginComponent />
                    </Col>
                    <Col xs={3}></Col>
                    <Col className="logoTce" xs={2}>
                        <img alt="" className="imgTceView" src={logoTCESP} />
                    </Col>
                    <hr className="hr" ></hr>
                </Row>

            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)


import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col} from 'react-bootstrap';
import './Style.css';
import { setTela } from './panelActions'
import btInserir from '../../components/img/bt-inserir-documento-off.png'
import btConsultar from '../../components/img/bt-consultar-documentos-on.png'
import btEdital from '../../components/img/bt-edital.png'
import btDecisao from '../../components/img/bt-decisao.png'
import btRepresentacao from '../../components/img/bt-representacao.png'
import btBack from '../img/bt-voltar-01b.png'



class Panelinicial extends Component {

    render() {
        return (
            <div>
                <div className="btnBack">
                        <button size="sm" variant="outline-primary" className="btnBackStyle" onClick={() => this.props.setTela("/panel")}><img className="btnVoltar" src={btBack}/></button>
                    </div>
                    <Row className="rowBtbBodyPanelInicial">
                        <Col xs={3}>
                            <img alt="" className="imgPanelTop" src={btInserir}></img>
                        </Col>
                        <Col xs={3}>
                            <img  alt=""  className="imgPanelTop" onClick={() => this.props.setTela("/lista")} src={btConsultar}></img>
                        </Col>
                        <Col xs={6}></Col>
                    </Row>
                    <Row className="rowOptions">
                        <Col xs={4}>
                            <img alt=""  className="imgPanel" onClick={() => this.props.setTela("/edital")} src={btEdital}></img>
                        </Col>
                        <Col xs={4}>
                            <img alt="" className="imgPanel" onClick={() => this.props.setTela("/representacao")} src={btRepresentacao}></img>
                        </Col>
                        <Col xs={4}>
                            <img alt=""  className="imgPanel" onClick={() => this.props.setTela("/decisao")} src={btDecisao}></img>
                        </Col>
                    </Row>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {

    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setTela}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Panelinicial)
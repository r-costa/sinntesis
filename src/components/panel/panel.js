import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {  Row, Col,Image } from 'react-bootstrap';
import './Style.css';
import btInserir from '../../components/img/bt-inserir-documentos.png'
import btConsultar from '../../components/img/bt-consultar-documentos.png'
import btInserirPc from '../../components/img/bt-inserir-palavras-chaves.png'
import btInserirLeis from '../../components/img/bt-inserir-leis.png'
import { setTela,changeNameLogin } from './panelActions'


class Panel extends Component {
    render() {
        return (
            <div>
                    <Row className="rowBtbBody">
                        <Col xs={5} >
                            <Image className="responsive-image__image" onClick={() => this.props.setTela("/inserir")} src={btInserir} ></Image>
                        </Col>
                        <Col xs={1}></Col>
                        <Col xs={5} >
                            <Image onClick={() => this.props.setTela("/lista")} className="responsive-image__image" src={btConsultar} ></Image>
                        </Col>
                    </Row>
                    <Row className="rowBtbBody">
                        <Col xs={5} >
                            <Image className="responsive-image__image" onClick={() => this.props.setTela("/termos")} src={btInserirPc} ></Image>
                        </Col>
                        <Col xs={1}></Col>
                        <Col xs={5} >
                            <Image className="responsive-image__image" onClick={() => this.props.setTela("/Leis")} src={btInserirLeis} ></Image>
                        </Col>
                    </Row>
            </div>
        )

    }
}

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ setTela,changeNameLogin }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Panel)
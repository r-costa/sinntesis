const INITIAL_STATE = {
    listaTermos: null, listaTermosAdicionados: [], inputDescricaoPopup: '', inputPalavraChavePopup: ''
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'GET_TERMO_DATA':
            return { ...state, listaTermos: action.payload }
        case 'REMOVE_INPUT_TERMOS':
            return {
                ...state,
                listaTermos: state.listaTermos.filter((item, index) => index !== action.payload)
            }
        case 'REMOVE_INPUT_TERMOS_ADICIONADOS':
            return {
                ...state,
                listaTermosAdicionados: state.listaTermosAdicionados.filter((item, index) => index !== action.payload)
            }
        case 'ADD_TERMOS_ADICIONADOS':
            return {
                ...state,
                listaTermosAdicionados: state.listaTermosAdicionados.concat(action.payload)
            }
        case 'CHANGE_PALAVRA_CHAVE':
            return {
                ...state,
                inputPalavraChavePopup: action.payload
            }
        case 'CHANGE_DESCRICAO':
            return {
                ...state,
                inputDescricaoPopup: action.payload
            }
        case 'CLEAR_ALL_TERMOS':
            return {
                state
            }    
        default:
            return INITIAL_STATE
    }
}